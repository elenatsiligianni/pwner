//
//  AppDelegate.h
//  Homepwner
//
//  Created by Elena Tsiligianni on 21/01/16.
//  Copyright © 2016 Elena Tsiligianni. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

