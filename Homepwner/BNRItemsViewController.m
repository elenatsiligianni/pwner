//
//  BNRItemsViewController.m
//  Homepwner
//
//  Created by Elena Tsiligianni on 21/01/16.
//  Copyright © 2016 Elena Tsiligianni. All rights reserved.
//

#import "BNRItemsViewController.h"
#import "BNRItemStore.h"
#import "BNRItem.h"
#import "BNRDetailViewController.h"

@interface BNRItemsViewController ()

// Replaced by the navigation bar buttons
//@property (nonatomic, strong) IBOutlet UIView *headerView;

@end

@implementation BNRItemsViewController

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
}

// Replaced by the navigation bar buttons

//-(UIView *)headerView {
//
//    // If you have not loaded the headerView yet...
//    if (!_headerView) {
//        
//        // Load HeaderView.xib
//        [[NSBundle mainBundle] loadNibNamed:@"HeaderView" owner:self options:nil];
//    }
//    
//    return _headerView;
//}

-(IBAction)addNewItem:(id)sender {
    
    // Make a new index path for the 0th section, last row
    // CRASH - if you don't add BNRItem before asking for row
    //NSInteger lastRow = [self.tableView numberOfRowsInSection:0];
    
    // Create a new BNRItem and add it to the store
    BNRItem *newItem = [[BNRItemStore sharedStore] createItem];
    
    // Figure out where that item is in the array
    NSInteger lastRow = [[[BNRItemStore sharedStore] allItems] indexOfObject:newItem];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:lastRow inSection:0];
    
     //Insert this new row into the table
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
 
// MORE SECTIONS
    
//    NSLog(@"%@", [[BNRItemStore sharedStore] itemsOver50]);
//    NSLog(@"%@", [[BNRItemStore sharedStore] itemsUnder50]);
    
//    if (newItem.valueInDollars > 50) {
//        
//        // Figure out where that item is in the array
//         int lastRow = [[[BNRItemStore sharedStore] itemsOver50] indexOfObject:newItem];
//         //NSLog(@"%i", lastRow);
//        
//         NSIndexPath *indexPath = [NSIndexPath indexPathForRow:lastRow inSection:0];
//        // Insert this new row into the table
//        [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
//    }
//    else {
//        // Figure out where that item is in the array
//        int lastRow = [[[BNRItemStore sharedStore] itemsUnder50] indexOfObject:newItem];
//        //NSLog(@"%i", lastRow);
//        
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:lastRow inSection:1];
//        
//        // Insert this new row into the table
//        [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationTop];
//    }

}

// Replaced by the navigation bar buttons

//-(IBAction)toggleEditingMode:(id)sender {
//
//    // If you are currently in editing mode...
//    if (self.isEditing) {
//        
//        // Change text or button to inform user of state
//        [sender setTitle:@"Edit" forState:UIControlStateNormal];
//        
//        // Turn off editing mode
//        [self setEditing:NO animated:YES];
//    }
//    else {
//        
//        // Change text of button to inform user of state
//        [sender setTitle:@"Done" forState:UIControlStateNormal];
//        
//        // Enter editing mode
//        [self setEditing:YES animated:YES];
//    }
//}

-(void)viewDidLoad {

    [super viewDidLoad];
    
    [self.tableView registerClass:[UITableViewCell class]
           forCellReuseIdentifier:@"UITableViewCell"];
    
    // Replaced by the navigation bar buttons
//    UIView *header = self.headerView;
//    [self.tableView setTableHeaderView:header];

    
     // BRONZE CHALLENGE *optional
    
    [self.tableView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:@"UITableViewHeaderFooterView"];
    
    
    // GOLD CHALLENGE
    
    UIImageView *bgImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iron"]];
    self.tableView.backgroundView = bgImageView;
    
    self.tableView.sectionFooterHeight = 44;
    self.tableView.sectionHeaderHeight = 50;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLineEtched];
    self.tableView.rowHeight = 60;
}

-(instancetype)init {
    
    // 1. Call the superclass's designated initializer
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {
        
        UINavigationItem *navItem = self.navigationItem;
        navItem.title = @"Homepwner";
        
        // Create a new bar button item that will send
        // addNewItem: to BNRItemsViewController
        UIBarButtonItem *bbi = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewItem:)];
        
        // Set this bar button item as the right item in the navigationItem
        navItem.rightBarButtonItem = bbi;
        
        navItem.leftBarButtonItem = self.editButtonItem;
        
        
//        for (int i = 0; i < 5; i++) {
//            [[BNRItemStore sharedStore] createItem];
//        }
    }
    return self;
}

// 2. Override the superclass's designated initializer to call mine
-(instancetype)initWithStyle:(UITableViewStyle)style {
    
    return [self init];
}

- (NSArray *)BNRDisplayStore
{
    // BNRDisplayArray asks for two arrays from BNRItemStore
    // One with items under 50 and one with items over 50
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    
// MORE SECTIONS
    
//    [temp addObject:[[BNRItemStore sharedStore] itemsOver50]];
//    [temp addObject:[[BNRItemStore sharedStore] itemsUnder50]];
    [temp addObject:[[BNRItemStore sharedStore] allItems]];
    
    return temp;
}

#pragma mark - DataSource methods

 // BRONZE CHALLENGE
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // **** WoooooooooooWWWWWWW *********
    // Count the number of arrays used to sort the items
    return [self.BNRDisplayStore count];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    //return [[[BNRItemStore sharedStore] allItems] count];
    
    // BRONZE CHALLENGE
    
    // Count the number of arrays in BNRDisplayStore to determine how many sections to create
    NSArray *itemSection = self.BNRDisplayStore[section];
    
    return [itemSection count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
   // Create an instance of UITableViewCell, with default appearance
//    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UITableViewCell"];
    
    // Get a new or recycled cell
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UITableViewCell"
                                                            forIndexPath:indexPath];

    
    // GOLD CHALLENGE
    cell.textLabel.font = [UIFont systemFontOfSize:20.0];
    
    // Set the text on the cell with the description of the item
    // that is at the nth index of items, where n = row this cell
    // will appear in on the tableview
    //NSArray *items = [[BNRItemStore sharedStore] allItems];

    
    // BNRItem *item = items[indexPath.row];
    //
    // cell.textLabel.text = [item description];
    
    
    // BRONZE CHALLENGE
    
    // Set the text on the cell with the description of the item
    // First determine which array to check based on the section
    // Then determine which item based on the row
    NSArray *itemSection = self.BNRDisplayStore[indexPath.section];
    BNRItem *item = itemSection[indexPath.row];
    cell.textLabel.text = [item description];

    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {

    // If the table view is asking to commit a delete command...
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSArray *items = [[BNRItemStore sharedStore] allItems];
        BNRItem *item = items[indexPath.row];
        [[BNRItemStore sharedStore] removeItem:item];
        
        // Also remove that row from the table view with an animation
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];

    }
    
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {

    [[BNRItemStore sharedStore] moveItemAtIndex:sourceIndexPath.row toIndex:destinationIndexPath.row];
}

// SILVER CHALLENGE Chapter 9
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BOOL isEditable = YES;
    NSUInteger count = [[[BNRItemStore sharedStore] allItems] count];
    
    if (indexPath.row == count - 1) {
        isEditable = NO;
    }
    
    return isEditable;
}


# pragma mark - Delegate methods

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BNRDetailViewController *detailViewController = [[BNRDetailViewController alloc] init];
    
    NSArray *items = [[BNRItemStore sharedStore] allItems];
    BNRItem *selectedItem = items[indexPath.row];
    
    // Give detail view controller a pointer to the item object in row
    detailViewController.item = selectedItem;
    
    // Push it onto the top of the navigation controller's stack
    [self.navigationController pushViewController:detailViewController animated:YES];
}

// BRONZE CHALLENGE Chapter 9
- (NSString *)tableView:(UITableView *)tableView
titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {

    return @"Remove";
}


// GOLD CHALLENGE Chapter 9
- (NSIndexPath *)tableView:(UITableView *)tableView
targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath
       toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
    
    NSArray *items = [[BNRItemStore sharedStore] allItems];
    
    // Define max row index (-1 since it's 0 based index)
    NSInteger maxRow = items.count - 1;
    
    // returns to its original position, else, allow the row to move by the returning the desired destination row.
    if (proposedDestinationIndexPath.row == maxRow) {
        NSLog(@"Destionation Row: %ld - not allowed", proposedDestinationIndexPath.row);
        return sourceIndexPath;
        
    }
    return proposedDestinationIndexPath;
}

// BRONZE CHALLENGE *optional

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UITableViewHeaderFooterView *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"UITableViewHeaderFooterView"];

    
    header.contentView.backgroundColor = [UIColor lightGrayColor];
    
    header.textLabel.text = @"Items";

// MORE SECTIONS 
    
//    if (section == 0) {
//        header.textLabel.text = @"Items > $50";
//    }
//    else {
//        header.textLabel.text = @"Items < $50";
//    }
    
    return header;
}

// BRONZE CHALLENGE *optional
//
//-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//
//    
//    UITableViewHeaderFooterView *footer = [tableView dequeueReusableHeaderFooterViewWithIdentifier:@"UITableViewHeaderFooterView"];
//    
//
//    footer.textLabel.text = @"This is a footer";
//    
//    return footer;
//
//}

@end
