//
//  BNRItemStore.h
//  Homepwner
//
//  Created by Elena Tsiligianni on 21/01/16.
//  Copyright © 2016 Elena Tsiligianni. All rights reserved.
//

#import <Foundation/Foundation.h>

// Directive telling the compiler that there is a BNRItem class and that it doesn't need to know the class's
// details in the current file, only that it exists.
@class BNRItem;

@interface BNRItemStore : NSObject

@property (nonatomic, readonly) NSArray *allItems;

// Notice that this is a class method and prefixed with a + instead of a -
+(instancetype)sharedStore;
-(BNRItem *)createItem;
-(void)removeItem:(BNRItem *)item;
-(void)moveItemAtIndex:(NSUInteger)fromIndex toIndex:(NSUInteger)toIndex;

// MORE SECTIONS

// New methods to return certain items - probably should have made these @property's
//- (NSArray *)itemsOver50;
//- (NSArray *)itemsUnder50;

@end
