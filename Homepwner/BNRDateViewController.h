//
//  BNRDateViewController.h
//  Homepwner
//
//  Created by Elena Tsiligianni on 25/01/16.
//  Copyright © 2016 Elena Tsiligianni. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BNRItem;

@interface BNRDateViewController : UIViewController

@property (nonatomic, strong) BNRItem *item;

@end
