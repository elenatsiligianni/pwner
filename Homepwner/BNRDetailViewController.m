//
//  BNRDetailViewController.m
//  Homepwner
//
//  Created by Elena Tsiligianni on 24/01/16.
//  Copyright © 2016 Elena Tsiligianni. All rights reserved.
//

@import AVFoundation;
@import AVKit;

#import "BNRDetailViewController.h"
#import "BNRItem.h"
#import "BNRDateViewController.h"
#import "BNRImageStore.h"
#import <MobileCoreServices/MobileCoreServices.h>

@interface BNRDetailViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *serialNumberField;
@property (weak, nonatomic) IBOutlet UITextField *valueField;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *trashButton;

@end

@implementation BNRDetailViewController

// SILVER CHALLENGE Chapter 11
- (IBAction)clearPicture:(id)sender {
    
    // Remove from image shared store
    [[BNRImageStore sharedStore] deleteImageForKey:self.item.itemKey];
    
    // Update imageview image
    self.imageView.image = [UIImage imageNamed:@"noimage"];
    
    // Disable Trash button
    self.trashButton.enabled = NO;
    
}

- (IBAction)backgroundTapped:(id)sender {
    
    [self.view endEditing:YES];
}

- (IBAction)takePicture:(id)sender {
    
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    //FOR THE MORE CURIOUS Chapter 11
    imagePicker.mediaTypes = @[(NSString *)kUTTypeImage, (NSString *)kUTTypeMovie];
    
    // If the device has a camera take a picture, otherwise just pick from photo library
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        // GOLD CHALLENGE Chapter 11
        UIImageView *crossHair = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        crossHair.image = [UIImage imageNamed:@"crosshair"];
        crossHair.alpha = 0.5f;
        crossHair.contentMode = UIViewContentModeCenter;
        imagePicker.cameraOverlayView = crossHair;
    }
    else {
        
        imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
    }
    
    // BRONZE CHALLENGE Chapter 11
    imagePicker.allowsEditing = YES;
    
    imagePicker.delegate = self;
    
    // Place image picker on the screen
    [self presentViewController:imagePicker animated:YES completion:nil];
}

- (IBAction)changeDate:(UIButton *)sender {

    BNRDateViewController *dVC = [[BNRDateViewController alloc] init];
    dVC.item = self.item;
    [self.navigationController pushViewController:dVC animated:YES];
}

// Replacing the synthesized setter to do something extra,have a navigationItem.title
-(void)setItem:(BNRItem *)item {

    _item = item;
    self.navigationItem.title = _item.itemName;
}

-(void)viewDidDisappear:(BOOL)animated {

    [super viewDidDisappear:animated];
    
    // Clear first responder
    [self.view endEditing:YES];
    
    // "Save" changes to item
    BNRItem *item = self.item;
    item.itemName = self.nameField.text;
    item.serialNumber = self.serialNumberField.text;
    item.valueInDollars = [self.valueField.text intValue];
}

-(void)viewWillAppear:(BOOL)animated {

    [super viewWillAppear:animated];
    
    BNRItem *item = self.item;
    self.nameField.text = item.itemName;
    self.serialNumberField.text = item.serialNumber;
    self.valueField.text = [NSString stringWithFormat:@"%d", item.valueInDollars];
    
    
    // You need an NSDateFormatter that will turn a date into a simple date string
    static NSDateFormatter *dateFormatter = nil;
    if (!dateFormatter) {
        dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateStyle = NSDateFormatterMediumStyle;
        dateFormatter.timeStyle = NSDateFormatterNoStyle;
    }
    
    // Use filtered NSDate object to set dateLabel contents
    self.dateLabel.text = [dateFormatter stringFromDate:item.dateCreated];
    
    NSString *imageKey = self.item.itemKey;
    
    // Get the image for its image key from the image store
    UIImage *imageToDisplay = [[BNRImageStore sharedStore] imageForKey:imageKey];
    
    // OPTIONAL Chapter 11
    if (imageToDisplay) {
        // Use that image to put on the screen in the imageView
        self.imageView.image = imageToDisplay;
        self.trashButton.enabled = YES;
    }
    else {
        
        // Use the default image
        self.imageView.image = [UIImage imageNamed:@"noimage"];
        self.trashButton.enabled = NO;
    }
    
//    // Use that image to put on the screen in the imageView
//    self.imageView.image = imageToDisplay;
    
    // BRONZE CHALLENGE Chapter 10
    self.valueField.keyboardType = UIKeyboardTypeNumberPad;
    
    
    // SILVER CHALLENGE Chapter 10
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackOpaque;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Return" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    self.valueField.inputAccessoryView = numberToolbar;
    
}

// SILVER CHALLENGE Chapter 10
-(void)doneWithNumberPad{
    
    [self.valueField resignFirstResponder];
}

# pragma mark - imagePicker Delegate methods

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    NSURL *mediaURL = info[UIImagePickerControllerMediaURL];
    
        // Make sure this device supports videos in its photo album
        if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum([mediaURL path])) {
            
//            // Save the video to the photos album
//            UISaveVideoAtPathToSavedPhotosAlbum([mediaURL path], nil, nil, nil);
//            
//            // Remove the video from the temporary directory
//            [[NSFileManager defaultManager] removeItemAtPath:[mediaURL path] error:nil];
            


            
            NSLog(@"Video");
        }
    
    else {

        // Get picked image from info dictionary
        //UIImage *image = info[UIImagePickerControllerOriginalImage];
        
        // BRONZE CHALLENGE Chapter 11
        // Replace the above to save the edited image
        UIImage *image = info[UIImagePickerControllerEditedImage];
        
        // Store the image in the BNRImageStore for this key
        [[BNRImageStore sharedStore] setImage:image forKey:self.item.itemKey];
        
        // Put that image onto the screen in our image view
        self.imageView.image = image;
        
    }
    
    // Take image picker off the screen - you MUST call the dismiss method
    [self dismissViewControllerAnimated:YES completion:nil];
    
//    
//    
//    
//    if (info[UIImagePickerControllerMediaURL]) {
//        NSURL *mediaURL = info[UIImagePickerControllerMediaURL];
//        if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum([mediaURL path])) {
//            UISaveVideoAtPathToSavedPhotosAlbum([mediaURL path], nil, nil, nil);
//            //        [[NSFileManager defaultManager] removeItemAtPath:[mediaURL path] error:nil];
//        }
//    } else {
//        UIImage *image = info[UIImagePickerControllerEditedImage];
//        //UIImage *image = info[UIImagePickerControllerOriginalImage];
//        [[BNRImageStore sharedStore] setImage:image forKey:self.item.itemKey];
//        self.imageView.image = image;
//    }

}

# pragma mark - textField Delegate methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField {

    [textField resignFirstResponder];
    return YES;
}

@end

