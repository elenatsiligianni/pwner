//
//  BNRDateViewController.m
//  Homepwner
//
//  Created by Elena Tsiligianni on 25/01/16.
//  Copyright © 2016 Elena Tsiligianni. All rights reserved.
//

#import "BNRDateViewController.h"
#import "BNRItem.h"

@interface BNRDateViewController ()

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@end

@implementation BNRDateViewController


-(void)viewDidDisappear:(BOOL)animated {
    
    [super viewDidDisappear:animated];
    
    // Clear first responder
    [self.view endEditing:YES];
    // "Save" changes to item
    self.item.dateCreated = self.datePicker.date;

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated]; // always call super first on this method
    
    // Date must be in the future - forcing  datepicker date to always be future 60 sec
    self.datePicker.minimumDate = [NSDate dateWithTimeIntervalSinceNow:60];
    
}


@end
