//
//  BNRImageStore.m
//  Homepwner
//
//  Created by Elena Tsiligianni on 27/01/16.
//  Copyright © 2016 Elena Tsiligianni. All rights reserved.
//

#import "BNRImageStore.h"

@interface BNRImageStore ()

@property (nonatomic, strong) NSMutableDictionary *dictionary;

@end

@implementation BNRImageStore

-(void)setImage:(UIImage *)image forKey:(NSString *)key {
    
    //[self.dictionary setObject:image forKey:key];
    self.dictionary[key] = image;
}

-(UIImage *)imageForKey:(NSString *)key {

    //return [self.dictionary objectForKey:key];
    return self.dictionary[key];
}

-(void)deleteImageForKey:(NSString *)key {
    
    if (!key) {
        
        return;
    }
    
    [self.dictionary removeObjectForKey:key];
}

+(instancetype)sharedStore {

    static BNRImageStore *sharedStore = nil;
    
    if (!sharedStore) {
        sharedStore = [[self alloc] initPrivate];
    }
    
    return sharedStore;
}

// Noone should call init
-(instancetype)init {
    
    @throw [NSException exceptionWithName:@"Singleton" reason:@"Use +[BNRImageStore sharedStore]" userInfo:nil];
    
    return nil;
}

// Secret designated initializer
-(instancetype)initPrivate {

    self = [super init];
    
    if (self) {
        
        _dictionary = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

@end
