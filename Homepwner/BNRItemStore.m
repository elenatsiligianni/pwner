//
//  BNRItemStore.m
//  Homepwner
//
//  Created by Elena Tsiligianni on 21/01/16.
//  Copyright © 2016 Elena Tsiligianni. All rights reserved.
//

#import "BNRItemStore.h"
#import "BNRItem.h"
#import "BNRImageStore.h"

@interface BNRItemStore ()

@property (nonatomic) NSMutableArray *privateItems;

// MORE SECTIONS

//@property (nonatomic) NSMutableArray *privateItemsAbove50;
//@property (nonatomic) NSMutableArray *privateItemsBelow50;

@end

@implementation BNRItemStore

-(void)moveItemAtIndex:(NSUInteger)fromIndex toIndex:(NSUInteger)toIndex {
    
    if (fromIndex == toIndex) {
        return;
    }
    
    // Get pointer to object being moved so you can re-insert it
    BNRItem *item = self.privateItems[fromIndex];
    
    // Remove item from array
    [self.privateItems removeObjectAtIndex:fromIndex];
    
    // Insert item in array at new location
    [self.privateItems insertObject:item atIndex:toIndex];
}

-(void)removeItem:(BNRItem *)item {
    
    NSString *key = item.itemKey;
    
    [[BNRImageStore sharedStore] deleteImageForKey:key];

    [self.privateItems removeObjectIdenticalTo:item];
}


-(BNRItem *)createItem {

    BNRItem *item = [BNRItem randomItem];

    //[self.privateItems addObject:item];
    
    // SILVER CHALLENGE Chapter 9
    // Make sure "No more items" is the last row
    [self.privateItems insertObject:item atIndex:self.privateItems.count - 1];
    
    return item;
}

-(NSArray *)allItems  {
    
    return self.privateItems;
}

// MORE SECTIONS

//- (NSArray *)itemsOver50
//{
//    // Fill with BNRItems based on value > 50
//    NSMutableArray *holdingArray = [[NSMutableArray alloc] init];
//    for (BNRItem *item in self.privateItems) {
//        if (item.valueInDollars > 50) {
//            [holdingArray addObject:item];
//        }
//    }
//    // No more items!
//    [holdingArray addObject:@"No more items"];
//    
//    return holdingArray;
//}
//
//- (NSArray *)itemsUnder50
//{
//    // Fill with BNRItems based on value < 50
//    NSMutableArray *holdingArray = [[NSMutableArray alloc] init];
//    for (BNRItem *item in self.privateItems) {
//        if (item.valueInDollars <= 50) {
//            [holdingArray addObject:item];
//        }
//    }
//    // No more items!
//    [holdingArray addObject:@"No more items"];
//    
//    return holdingArray;
//}

+(instancetype)sharedStore {
    
    static BNRItemStore *sharedStore = nil;
    
    // Do I need to create a sharedStore?
    if (!sharedStore) {
        sharedStore = [[self alloc] initPrivate];
    }
    
    return sharedStore;

}

// If a programmer calls [[BNRItemStore alloc] init] let him
// know the error of his ways
-(instancetype)init {

    @throw [NSException exceptionWithName:@"Singleton" reason:@"Use +[BNRItemStore sharedStore]" userInfo:nil];
    return nil;
}

// Here is the real(secret) initializer
-(instancetype)initPrivate {

    self = [super init];
    
    if (self) {
        
        self.privateItems = [[NSMutableArray alloc] init];
        
        // SILVER CHALLENGE Chapter 9
        /// adding a last row of 'no more items!' during initialization
        [self.privateItems addObject:@"No more items!"];

// MORE SECTIONS        
        
//        self.privateItemsAbove50= [[NSMutableArray alloc]init];
//        self.privateItemsBelow50 = [[NSMutableArray alloc]init];
    }
    
    return self;
}

@end
